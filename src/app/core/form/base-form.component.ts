import { FormGroup } from '@angular/forms';

export class BaseFormComponent {

    public registerForm: FormGroup;
    public submitted = false;
    public isEditar: boolean;

    constructor() { }

    get form() {
        return this.registerForm.controls;
    }
}