import { CanDeactivate } from '@angular/router';

export class FormGuard implements CanDeactivate<any> {

    canDeactivate(
        component: any,
        //route: ActivatedRouteSnapshot,
        //state: RouterStateSnapshot
    ): boolean {
        if (component.registerForm.touched && !component.submitted) {
            return window.confirm("Suas alterações serão perdidas! Tem certeza que deseja sair dessa página?");
        }
        return true;
    }

}