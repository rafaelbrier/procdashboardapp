import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { HeaderPage } from './header/header.page';
import { HeaderRoutingModule } from './header.routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    //Routing
    HeaderRoutingModule
  ],
  declarations: [HeaderPage]
})
export class HeaderModule {}
