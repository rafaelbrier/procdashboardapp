import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { RunBookPage } from './run-book/run-book.page';
import { RunBookRoutingModule } from './run-book.routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    //Routing
    RunBookRoutingModule
  ],
  declarations: [RunBookPage]
})
export class RunBookModule {}
