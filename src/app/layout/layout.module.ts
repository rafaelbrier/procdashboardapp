import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

//import { FormGuard } from '../core/guards/form.guard';
import { LayoutRoutingModule } from './layout.routing.module';
import { LayoutComponent } from './layout.component';
import { TemplateModule } from '../core/template/template.module';

@NgModule({
    declarations: [LayoutComponent],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        //Routing
        LayoutRoutingModule,
        //Template
        TemplateModule
    ],
    providers: [
        //FormGuard
    ]
})
export class LayoutModule { }
