import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ProcLogPage } from './proc-log/proc-log.page';
import { ProcLogRoutingModule } from './proc-log.routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    //Routing
    ProcLogRoutingModule
  ],
  declarations: [ProcLogPage]
})
export class ProcLogModule {}
